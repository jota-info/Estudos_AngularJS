# coding: utf-8
from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def index():
    """Renderiza página que contém o app da API"""
    return render_template('index.html')

@app.route('/categorias')
def categorias():
    """Renderiza página de categorias"""
    return render_template('categorias.html')


if __name__ == "__main__":
    app.run(debug=True, port=4000)
