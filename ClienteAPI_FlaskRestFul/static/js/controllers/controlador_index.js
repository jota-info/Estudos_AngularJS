app.controller('controlador_index', function($scope, $http) {

        $scope.categorias = [];
        $scope.tarefas = [];

        var atualizarcategorias = function(){
          $http.get("http://localhost:5000/categorias").success(function(data){
          $scope.categorias = data;
          });
        };
        atualizarcategorias();

        var atualizartarefas = function(){
        $http.get("http://localhost:5000/categorias/todos").success(function(data){
          $scope.tarefas = data;
          });
        };
        atualizartarefas();

        $scope.addTarefa = function(tarefa) {
          $http.post("http://localhost:5000/categorias/todos", tarefa).success(function (data) {
            delete $scope.tarefa;
            atualizartarefas();
          });
        };

        $scope.delTarefas = function(tarefas) {
          $scope.tarefas = tarefas.filter(function(tarefa){
            if(tarefa.selecionado){
              $http.delete("http://localhost:5000/categorias/todos/"+tarefa.id);
            }else if(!tarefa.selecionado){
              return tarefa; }
          });
        };

      $scope.ordenadorVia = function(campo){
        $scope.criterio_ordenar = campo;
        $scope.AscDesc = !$scope.AscDesc;
      };
});
