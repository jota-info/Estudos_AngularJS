app.controller('controladorToDo', function($scope, $http) {

        $scope.tarefas = [];

        var atualizar = function(){
          $http.get("http://localhost:5000/todos").success(function(data){
            $scope.tarefas = data;
          });
        };

        $scope.addTarefa = function(tarefa) {
          $http.post("http://localhost:5000/todos", tarefa).success(function (data) {
            delete $scope.tarefa;
            atualizar();
          });
        };

        $scope.delTarefas = function(tarefas) {
          $scope.tarefas = tarefas.filter(function(tarefa){
            if(tarefa.selecionado){
              $http.delete("http://localhost:5000/todos/"+tarefa.id);
            }else if(!tarefa.selecionado){
              return tarefa; }
          });
        };

      $scope.ordenadorVia = function(campo){
        $scope.criterio_ordenar = campo;
        $scope.AscDesc = !$scope.AscDesc;
      };

  atualizar();
});


/*  Adicionar caso achar necessário update.    */
//
// $scope.updTarefas = function(tarefas, alter) {
//   $scope.tarefas = tarefas.filter(function(tarefa){
//     if(tarefa.selecionado){
//       $http.put("http://localhost:5000/todos/"+tarefa.id, alter);
//       atualizar();
//       $scope.tarefas = alter;
//     }
//   });
// };
