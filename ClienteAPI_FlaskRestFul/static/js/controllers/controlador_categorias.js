app.controller('controlador_categorias', function($scope, $http) {

        $scope.categorias = [];

        var atualizar = function(){
          $http.get("http://localhost:5000/categorias").success(function(data){
            $scope.categorias = data;
          });
        };

        $scope.addCategoria = function(categoria) {
          $http.post("http://localhost:5000/categorias", categoria).success(function (data) {
            delete $scope.categoria;
            atualizar();
          });
        };

        $scope.delCategoria = function(categorias) {
          $scope.categorias = categorias.filter(function(categoria){
            if(categoria.selecionado){
              $http.delete("http://localhost:5000/categorias/"+categoria.nome);
            }else if(!categoria.selecionado){
              return categoria; }
          });
        };

      $scope.ordenadorVia = function(campo){
        $scope.criterio_ordenar = campo;
        $scope.AscDesc = !$scope.AscDesc;
      };

  atualizar();
});
